﻿using Example.GameObjects;
using System.Collections.Generic;
using Ventillo;
using Ventillo.GameObjects;
using Ventillo.Interfaces;
using Ventillo.System;

namespace Example.Scenes
{
    internal class BasicScene : IScene
    {
        public List<GameObject> GameObjects { get; set; } = new();

        internal BasicScene()
        {
            Engine.Logger.Info("Test");
            Engine.Logger.Debug("Test");
            Engine.Logger.Warn("Test");
            Engine.Logger.Error("Test", null);

            Engine.Logger.Info("Creating Square Object");
            var square = new Square(new Vector(Engine.PlayableArea.Max.X / 2, Engine.PlayableArea.Max.Y / 2));
            Engine.Logger.Info("Created Square Object", square);
            GameObjects.Add(square);
        }
        ~BasicScene()
        {
            GameObjects.ForEach(gameObject => DeleteGameObject(gameObject));
        }

        public void DeleteGameObject(GameObject gameObject)
        {
            gameObject = null;
            GameObjects.Remove(gameObject);
        }

        public void CheckDelete()
        {
            GameObjects.FindAll(gameObject => gameObject.ToDelete).ForEach(gameObject => DeleteGameObject(gameObject));
        }

        public void Update()
        {
            GameObjects.ForEach(gameObject => gameObject.Update(GameObjects));
            CheckDelete();
        }

        public void Draw()
        {
            GameObjects.ForEach(gameObject => gameObject.Draw());
        }
    }
}
