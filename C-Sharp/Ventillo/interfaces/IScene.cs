﻿using System.Collections.Generic;
using Ventillo.GameObjects;

namespace Ventillo.Interfaces
{
    public interface IScene
    {
        List<GameObject> GameObjects { get; set; }
        void CheckDelete();
        void DeleteGameObject(GameObject gameObject);
        void Update();
        void Draw();
    }
}
