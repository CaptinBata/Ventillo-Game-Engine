﻿using Ventillo.System;

namespace Ventillo.GameObjects.Interfaces
{
    internal interface ITransform
    {
        Vector Position { get; set; }
        Vector ToGlobalCoords(Vector localVector);
        Vector ToLocalCoords(Vector globalVector);
    }
}
