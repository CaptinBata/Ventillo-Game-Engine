﻿using System.Collections.Generic;
using Ventillo.System;

namespace Ventillo.GameObjects.Interfaces
{
    internal interface IDrawObject
    {
        List<Vector> DrawPoints { get; set; }
        Colour FillColour { get; set; }
        Colour StrokeColour { get; set; }
        MinMax? MinMax { get; set; }
        string? Text { get; set; }
    }
}
