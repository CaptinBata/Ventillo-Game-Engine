﻿using Ventillo.GameObjects;

namespace Ventillo.Debug.Interfaces
{
    public interface IDebugObject
    {
        void Update(double timestamp);
        double GetFPS();
        void Draw();
        void DrawObjectBounds(GameObject objectToDraw);
    }
}
