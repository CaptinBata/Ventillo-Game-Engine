﻿using System;
using System.Runtime.CompilerServices;
using SFML.Graphics;

[assembly: InternalsVisibleTo("TestVentillo")]

namespace Ventillo.System
{
    public class Colour
    {
        Color sfmlColor;

        public Colour() { }
        public Colour(int red, int green, int blue, int alpha = 255)
        {
            sfmlColor = new Color(Convert.ToByte(red), Convert.ToByte(green), Convert.ToByte(blue), Convert.ToByte(alpha));
        }

        internal Color GetSFMLColor()
        {
            return sfmlColor;
        }
    }
}
