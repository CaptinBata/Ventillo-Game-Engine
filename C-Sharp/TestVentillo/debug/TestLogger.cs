﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;

using Ventillo.Debug;
using Ventillo.Utils;

namespace TestVentillo.Debug
{
    internal class TestLogger
    {
        StringWriter stringWriter;

        [SetUp]
        public void Setup()
        {
            stringWriter = new StringWriter();
            Console.SetOut(stringWriter);
            stringWriter.Flush();
        }

        [Test]
        public void TestDebug_WritesToConsoleWithoutDataParam()
        {
            var testLogger = new Logger(LoggerLevels.DEBUG);

            var message = "Test message for the console";

            testLogger.Debug(message);

            Assert.AreEqual($"DEBUG: {message}" + stringWriter.NewLine, stringWriter.ToString());
        }

        [Test]
        public void TestDebug_WritesToConsoleWithDataObjectParamAndCallsToObjectString()
        {
            var testLogger = new Logger(LoggerLevels.DEBUG);

            var message = "Test message for the console";
            var data = new List<string>() { "Hello", "Test", "World" };

            testLogger.Debug(message, data);

            Assert.AreEqual($"DEBUG: {message}, {data.ToObjectString()}" + stringWriter.NewLine, stringWriter.ToString());
        }

        [Test]
        public void TestDebug_DoesntWriteToConsoleIfLoggerLevelAboveLimit()
        {
            var testLogger = new Logger(LoggerLevels.INFO);

            var message = "Test message for the console";
            var data = new List<string>() { "Hello", "Test", "World" };

            testLogger.Debug(message);
            testLogger.Debug(message, data);

            Assert.AreEqual("", stringWriter.ToString());
        }

        [Test]
        public void TestInfo_WritesToConsoleWithoutDataParam()
        {
            var testLogger = new Logger(LoggerLevels.INFO);

            var message = "Test message for the console";

            testLogger.Info(message);

            Assert.AreEqual($"INFO: {message}" + stringWriter.NewLine, stringWriter.ToString());
        }

        [Test]
        public void TestInfo_WritesToConsoleWithDataObjectParamAndCallsToObjectString()
        {
            var testLogger = new Logger(LoggerLevels.INFO);

            var message = "Test message for the console";
            var data = new List<string>() { "Hello", "Test", "World" };

            testLogger.Info(message, data);

            Assert.AreEqual($"INFO: {message}, {data.ToObjectString()}" + stringWriter.NewLine, stringWriter.ToString());
        }

        [Test]
        public void TestInfo_WritesToConsoleIfLoggerLevelBelowLimitButNotEqual()
        {
            var testLogger = new Logger(LoggerLevels.DEBUG);

            var message = "Test message for the console";
            var data = new List<string>() { "Hello", "Test", "World" };

            testLogger.Info(message, data);

            Assert.AreEqual($"INFO: {message}, {data.ToObjectString()}" + stringWriter.NewLine, stringWriter.ToString());
        }

        [Test]
        public void TestInfo_DoesntWriteToConsoleIfLoggerLevelAboveLimit()
        {
            var testLogger = new Logger(LoggerLevels.WARN);

            var message = "Test message for the console";
            var data = new List<string>() { "Hello", "Test", "World" };

            testLogger.Info(message);
            testLogger.Info(message, data);

            Assert.AreEqual("", stringWriter.ToString());
        }

        [Test]
        public void TestWarn_WritesToConsoleWithoutDataParam()
        {
            var testLogger = new Logger(LoggerLevels.WARN);

            var message = "Test message for the console";

            testLogger.Warn(message);

            Assert.AreEqual($"WARN: {message}" + stringWriter.NewLine, stringWriter.ToString());
        }

        [Test]
        public void TestWarn_WritesToConsoleWithDataObjectParamAndCallsToObjectString()
        {
            var testLogger = new Logger(LoggerLevels.WARN);

            var message = "Test message for the console";
            var data = new List<string>() { "Hello", "Test", "World" };

            testLogger.Warn(message, data);

            Assert.AreEqual($"WARN: {message}, {data.ToObjectString()}" + stringWriter.NewLine, stringWriter.ToString());
        }

        [Test]
        public void TestWarn_WritesToConsoleIfLoggerLevelBelowLimitButNotEqual()
        {
            var testLogger = new Logger(LoggerLevels.INFO);

            var message = "Test message for the console";
            var data = new List<string>() { "Hello", "Test", "World" };

            testLogger.Warn(message, data);

            Assert.AreEqual($"WARN: {message}, {data.ToObjectString()}" + stringWriter.NewLine, stringWriter.ToString());
        }

        [Test]
        public void TestWarn_DoesntWriteToConsoleIfLoggerLevelAboveLimit()
        {
            var testLogger = new Logger(LoggerLevels.ERROR);

            var message = "Test message for the console";
            var data = new List<string>() { "Hello", "Test", "World" };

            testLogger.Warn(message);
            testLogger.Warn(message, data);

            Assert.AreEqual("", stringWriter.ToString());
        }

        [Test]
        public void TestError_WritesToConsoleWithoutDataParam()
        {
            var testLogger = new Logger(LoggerLevels.ERROR);

            var message = "Test message for the console";
            var error = new Exception("Test exception");

            testLogger.Error(message, error);

            Assert.AreEqual($"ERROR: {message}, {error}" + stringWriter.NewLine, stringWriter.ToString());
        }

        [Test]
        public void TestError_WritesToConsoleWithDataObjectParamAndCallsToObjectString()
        {
            var testLogger = new Logger(LoggerLevels.ERROR);

            var message = "Test message for the console";
            var error = new Exception("Test exception");
            var data = new List<string>() { "Hello", "Test", "World" };

            testLogger.Error(message, error, data);

            Assert.AreEqual($"ERROR: {message}, {error} - {data.ToObjectString()}" + stringWriter.NewLine, stringWriter.ToString());
        }

        [Test]
        public void TestError_WritesToConsoleIfLoggerLevelBelowLimitButNotEqual()
        {
            var testLogger = new Logger(LoggerLevels.WARN);

            var message = "Test message for the console";
            var error = new Exception("Test exception");
            var data = new List<string>() { "Hello", "Test", "World" };

            testLogger.Error(message, error, data);

            Assert.AreEqual($"ERROR: {message}, {error} - {data.ToObjectString()}" + stringWriter.NewLine, stringWriter.ToString());
        }
    }
}
