﻿using NUnit.Framework;
using FluentAssertions;

using Ventillo.System;

namespace TestVentillo.System
{
    public class TestVector
    {
        [SetUp]
        public void Setup()
        {

        }

        [Test]
        [TestCase(0, 0, 10, 10, ExpectedResult = "10, 10")]
        [TestCase(100, 123, 23, 333, ExpectedResult = "123, 456")]
        [TestCase(0, -100, 0, 190, ExpectedResult = "0, 90")]
        [TestCase(-10, 10, 0, 0, ExpectedResult = "-10, 10")]
        [TestCase(100, 100, -100, -100, ExpectedResult = "0, 0")]
        public string TestStaticTranslate_ReturnsNewVectorWithTransformation(double vectorX, double vectorY, double newVectorX, double newVectorY)
        {
            var translated = Vector.Translate(new Vector(vectorX, vectorY), new Vector(newVectorX, newVectorY));
            var x = translated.X.ToString();
            var y = translated.Y.ToString();
            return $"{x}, {y}";
        }

        [Test]
        [TestCase(0, 0, 10, 10, ExpectedResult = "10, 10")]
        [TestCase(100, 123, 23, 333, ExpectedResult = "123, 456")]
        [TestCase(0, -100, 0, 190, ExpectedResult = "0, 90")]
        [TestCase(-10, 10, 0, 0, ExpectedResult = "-10, 10")]
        [TestCase(100, 100, -100, -100, ExpectedResult = "0, 0")]
        public string TestTranslate_UpdatesVectorWithTransformation(double vectorX, double vectorY, double newVectorX, double newVectorY)
        {
            var vector = new Vector(vectorX, vectorY);
            vector.Translate(new Vector(newVectorX, newVectorY));
            var x = vector.X.ToString();
            var y = vector.Y.ToString();
            return $"{x}, {y}";
        }

        [Test]
        [TestCase(0, 0, 10, 10, 10, ExpectedResult = "0001.8884, -0001.5846")]
        [TestCase(100, 100, 10, 10, 57, ExpectedResult = "-0016.4628, 0134.4979")]
        [TestCase(-100, 100, 134, 17, 57, ExpectedResult = "-0063.0552, -0134.0439")]
        [TestCase(1000, -1, 420, 69, 360, ExpectedResult = "1000.0000, -0001.0000")]
        [TestCase(42, 233, 90, 400, -48, ExpectedResult = "-0066.2235, 0323.9261")]
        [TestCase(-70, -90, -60, -432, -270, ExpectedResult = "-0402.0000, -0442.0000")]
        public string TestStaticRotateVectorAroundPoint_TransformsVectorAroundPoint(double vectorX, double vectorY, double pointX, double pointY, double angle)
        {
            var result = Vector.RotateVectorAroundPoint(new Vector(vectorX, vectorY), new Vector(pointX, pointY), angle);
            var roundedX = result.X.ToString("0000.0000");
            var roundedY = result.Y.ToString("0000.0000");

            return $"{roundedX}, {roundedY}";
        }

        [Test]
        [TestCase(0, 0, 10, 10, 10, ExpectedResult = "0001.8884, -0001.5846")]
        [TestCase(100, 100, 10, 10, 57, ExpectedResult = "-0016.4628, 0134.4979")]
        [TestCase(-100, 100, 134, 17, 57, ExpectedResult = "-0063.0552, -0134.0439")]
        [TestCase(1000, -1, 420, 69, 360, ExpectedResult = "1000.0000, -0001.0000")]
        [TestCase(42, 233, 90, 400, -48, ExpectedResult = "-0066.2235, 0323.9261")]
        [TestCase(-70, -90, -60, -432, -270, ExpectedResult = "-0402.0000, -0442.0000")]
        public string TestRotateVectorAroundPoint_TransformsVectorAroundPoint(double vectorX, double vectorY, double pointX, double pointY, double angle)
        {
            var vector = new Vector(vectorX, vectorY);
            vector.RotateVectorAroundPoint(new Vector(pointX, pointY), angle);
            var roundedX = vector.X.ToString("0000.0000");
            var roundedY = vector.Y.ToString("0000.0000");

            return $"{roundedX}, {roundedY}";
        }
    }
}
