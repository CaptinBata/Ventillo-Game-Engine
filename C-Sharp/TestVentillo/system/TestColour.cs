﻿using NUnit.Framework;

using System;

using SFML.Graphics;

using Ventillo.System;

namespace TestVentillo.System
{
    public class TestColour
    {
        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void TestGetSFMLColor_ReturnsSFMLColourWithoutSettingAlpha()
        {
            var testColour = new Colour(100, 100, 100);
            var expectedColour = new Color(Convert.ToByte(100), Convert.ToByte(100), Convert.ToByte(100));

            Assert.AreEqual(expectedColour, testColour.GetSFMLColor());
        }

        [Test]
        public void TestGetSFMLColor_ReturnsSFMLColourWithSettingAlpha()
        {
            var testColour = new Colour(100, 100, 100, 100);
            var expectedColour = new Color(Convert.ToByte(100), Convert.ToByte(100), Convert.ToByte(100), Convert.ToByte(100));

            Assert.AreEqual(expectedColour, testColour.GetSFMLColor());
        }
    }
}
