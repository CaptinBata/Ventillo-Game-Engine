﻿using NUnit.Framework;

using Ventillo.System;

namespace TestVentillo.System
{
    public class TestMinMax
    {
        [SetUp]
        public void Setup()
        {

        }

        [Test]
        [TestCase(0, 0, 400, 400, 200, 200, ExpectedResult = true)]
        [TestCase(0, 0, 400, 400, 0, 0, ExpectedResult = false)]
        [TestCase(0, 0, 400, 400, 600, 200, ExpectedResult = false)]
        [TestCase(0, 0, 400, 400, 100, 350, ExpectedResult = true)]
        [TestCase(0, 0, 1, 1, 200, 200, ExpectedResult = false)]
        [TestCase(-100, -100, 1, 1, -50, 0, ExpectedResult = true)]
        [TestCase(-1000, -1000, -10, -100, 200, 200, ExpectedResult = false)]
        public bool TestPointIntersects_ReturnsWhetherPointIntersects(int minX, int minY, int maxX, int maxY, int pointX, int pointY)
        {
            var area = new MinMax(new Vector(minX, minY), new Vector(maxX, maxY));
            var point = new Vector(pointX, pointY);

            return area.PointIntersects(point);
        }
    }
}
